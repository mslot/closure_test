fn main() {
    let x = String::from("hello");
    let c = || x;
    let z = |x| println!("many {}", x);

    consume_once(c);
    consume_many(z);
    consume_once_println(z);
}

fn consume_once_println<F>(function: F)
where
    F: FnOnce(String),
{
    function(String::from("hello 1"));
    //function(String::from("hello 1")); //cant even compile: FnOnce mean "can only be used once"
}

fn consume_many<F>(function: F)
where
    F: Fn(String),
{
    function(String::from("hello 1"));
    function(String::from("hello 2")); //can be called many times
}

fn consume_once<F>(function: F)
where
    F: FnOnce() -> String,
{
    println!("{}", function());

    //function(); //cant even compile: FnOnce mean "can only be used once"
}
